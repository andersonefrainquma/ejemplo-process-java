package Controlador;

import Modelo.SysCall;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Efranor
 */
public class Test {

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        SysCall s = new SysCall();

        boolean control = false;
        do {
            System.out.println("Bienvenido, seleccione el test a ejecutar: ");
            System.out.println("1)Mostrar fecha actual\n2)Mostrar arbol de directorios\n3)Ping a Google");
            short eleccion = sc.nextShort();

            switch (eleccion) {
                case 1 -> { //Fecha
                    String comando[] = {"cmd.exe", "/c", "Date /T"};
                    s.crear_abrirArchivo("La fecha actual es: ", "src/vista/fecha.txt", comando);
                    s.cambiarSalida(true);
                }
                case 2 -> { //Arbol
                    System.out.print("Esto puede demorar un poco, por favor sea paciente.");
                    String comando[] = {"cmd.exe", "/c", "tree", "/a", "C:\\"};
                    s.crear_abrirArchivo("El arbol de directorios es: ", "src/vista/arbol.txt", comando);
                    s.cambiarSalida(true);
                }
                case 3 -> { //Ping
                    System.out.print("Cantidad de ping que desea hacer: ");
                    short n = sc.nextShort();
                    if (n <= 0) {
                        System.out.println("Error, seleccione una cantidad >= 1");
                    } else {
                        String comando[] = {"cmd.exe", "/c", "ping -n " + n + " google.com"};

                        s.crearArchivo("", "src/vista/ping.txt", comando);

                        String resultado = s.getBuffer();
                        Scanner scanner = new Scanner(resultado);
                        String linea = scanner.nextLine();

                        if (linea.contains("encontrar")) {
                            s.crearArchivo("Error, no hay internet", "src/vista/ping.txt");
                            s.abrirArchivo("src/vista/ping.txt");
                            s.cambiarSalida(true);
                        } else {
                            List<Integer> tiempos = new ArrayList<>();
                            String lineaEjemplo = "";
                            while (scanner.hasNextLine()) {
                                linea = scanner.nextLine();
                                if (linea.contains("tiempo")) {
                                    tiempos.add(tiempo(linea));
                                    lineaEjemplo = linea;
                                }
                            }
                            scanner.close();

                            List<String> tiemposMayores = obtenerMayores(tiempos, lineaEjemplo);
                            resultado = "El (los) pin(es) con más demora:\n" + tiemposMayores;
                            System.out.println(resultado);
                            s.abrirArchivo("src/vista/ping.txt");
                            s.cambiarSalida(true);
                            System.out.println(s.getBuffer() + "\n" + resultado);
                        }
                        s.cerrarProceso();
                    }
                    s.limpiarBuffer();
                }
                default -> {
                    System.out.println("No existe está opción.");
                }
            }
            System.out.println("\n¿Desea ejecutar otro Test?(0-No; 1-Si)");
            control = sc.nextByte() == 1;
        } while (control);
    }

    private static int tiempo(String linea) {
        String patron = "tiempo=(\\d+)";
        Pattern pattern = Pattern.compile(patron);
        Matcher matcher = pattern.matcher(linea);

        return matcher.find() ? Integer.parseInt(matcher.group(1)) : 0;
    }

    private static List<String> obtenerMayores(List<Integer> tiempos, String linea) {
        List<String> mayores = new ArrayList<>();
        int mayor = 0;

        for (int numero : tiempos) {
            if (numero > mayor) {
                mayor = numero;
                mayores.clear();
                mayores.add(reemplazarTiempo(linea, numero));
            } else if (numero == mayor) {
                mayores.add(reemplazarTiempo(linea, numero));
            }
        }

        return mayores;
    }

    private static String reemplazarTiempo(String cadena, int tiempo) {
        String tiempoStr = "tiempo=" + tiempo;
        return cadena.replaceFirst("tiempo=\\d+", tiempoStr);
    }
}
