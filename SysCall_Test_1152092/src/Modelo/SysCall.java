package Modelo;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madarme
 */
public class SysCall {

    private Process p = null;
    private String buffer = "";
    private FileOutputStream salida = null;

    public SysCall() {
    }

    public String run(String[] command) {

        try {
            p = Runtime.getRuntime().exec(command);

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return "nada";
    }

    private String getOut() throws UnsupportedEncodingException {
        BufferedReader is = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        try {
            // reading the output
            while ((line = is.readLine()) != null) {
                buffer += line + "\n";

            }
        } catch (IOException ex) {
            Logger.getLogger(SysCall.class.getName()).log(Level.SEVERE, null, ex);
        }
        return buffer;
    }

    public void printer() throws UnsupportedEncodingException {
        System.out.println(this.getOut());
    }

    public void crear_abrirArchivo(String mensaje, String direccion, String comando[]) throws IOException {
        crearArchivo(mensaje, direccion, comando);
        abrirArchivo(direccion);
        this.salida.close();
        this.limpiarBuffer();
    }

    public void crearArchivo(String mensaje, String direccion, String comando[]) throws FileNotFoundException, IOException {
        crearArchivo(mensaje, direccion);
        this.run(comando);
        this.printer();
    }

    public void crearArchivo(String mensaje, String direccion) throws FileNotFoundException {
        salida = new FileOutputStream(direccion);
        //Cambiar la salida estándar:
        System.setOut(new PrintStream(salida));
        System.out.write(mensaje.getBytes(), 0, mensaje.length());
    }

    public void abrirArchivo(String dir) throws FileNotFoundException, IOException {
        File direccion = new File(dir);
        String comando[] = {"notepad.exe", direccion.getAbsolutePath()};
        this.run(comando);
    }

    public String getBuffer() {
        return buffer;
    }

    public void limpiarBuffer() {
        this.buffer = "";
    }

    public void cambiarSalida(boolean eleccion) {
        if (eleccion) { //Defecto
            System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
        } else { //Archivo
            System.setOut(new PrintStream(salida));
        }
    }

    public void cerrarProceso() throws IOException {
        this.salida.close();
    }
}
